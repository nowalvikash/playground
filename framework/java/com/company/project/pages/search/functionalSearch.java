package com.company.project.pages.search;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.org.e2e.config.SeleniumTest;

public class functionalSearch extends SeleniumTest {
	
	private searchPage searchpage;
	
	@BeforeClass(alwaysRun = true)
	public void beforeClass() throws MalformedURLException, InterruptedException {
		
		searchpage= PageFactory.initElements(driver, searchPage.class);
	}
	
	@Test(priority = 1, description = "Verify that user is able to search.\n")
	public void verify_that_user_is_able_to_search() {
		System.out.println("Inside verify_that_user_is_able_to_search.");
		searchpage
		.search();
	}

}
