package com.company.project.pages.search;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;

import com.org.e2e.util.Page;

public class searchPage extends Page<searchPage> {
	
	private static final String ROUTE = "/";
	private static final String SEARCHBOX = ".gLFyf.gsfi";
	private static final String GOOGLESEARCH = "btnK";
	
	
	@FindBy(css =SEARCHBOX )
	private WebElement searchBox;
	@FindBy(name =GOOGLESEARCH )
	private WebElement googlesearch;
	
	public searchPage(WebDriver driver) {
		super(driver, ROUTE);
	}

	@Override
	public searchPage verify_route() {
		return this;
	}
	
	public searchPage search() {
	//	String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		searchBox.sendKeys("car");
		return this;
	}
	

}
