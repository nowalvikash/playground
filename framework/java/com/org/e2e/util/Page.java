package com.org.e2e.util;

import static org.openqa.selenium.support.ui.ExpectedConditions.urlMatches;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.org.e2e.config.SeleniumTest;

/**
 * Base page object for ui pages.
 * <p>
 * A page object represents a page and includes hooks to its IDs, elements,
 * fields and more importantly implements all functionality that can be
 * performed on a page.
 * 
 * @param <T>
 *           Page object type. Example: _LoginPage
 */
public abstract class Page<T> {

   private static final String NAVBAR_USER_ID = "navbar-user-a";
   private static final String LOGOUT_ID = "navbar-logout-li";
   private static final String HELP_LINK_ID = "page-help-icon";
   private static final String LOADING_INDICATOR = ".loading-indicator";

   protected String URL_PATH;
   protected WebDriver driver;

   @FindBy(id = LOGOUT_ID)
   private WebElement logoutButton;

   @FindBy(id = HELP_LINK_ID)
   private WebElement helpButton;

   /**
    * @param driver
    *           the webdriver
    * @param route
    *           a partial route for the page. Example '/login'
    */
   public Page(WebDriver driver, String route) {
      this.driver = driver;
      this.URL_PATH = route;
   }

   /**
    * A method which can be used to navigate to the page.
    * <p>
    * If navigation fails, anh exception will be thrown.
    * 
    * @return The page object representing the result of the route.
    */
   public T navigate_to_route() {
      report_step(generate_step_descriptor() + ", route: " + URL_PATH);
      driver.get(build_qualified_url(URL_PATH));
      System.out.println("####### Current URL::"+ driver.getCurrentUrl());
      return verify_route();
   }

   /**
    * Use this method to navigate to the page if it's possible that you are
    * already on the page and don't absolutely need the page reloaded.
    * 
    * @return the page
    */
   public T navigate_to_route_if_not_already_there() {
      final String step = generate_step_descriptor();

      final String currentUrl = driver.getCurrentUrl();
      if (currentUrl.endsWith(URL_PATH)) {
         report_step(step + ", already on page, not reloading");
      } else {
         report_step(step + ", route: " + URL_PATH);
         driver.get(build_qualified_url(URL_PATH));
      }
      return verify_route();
   }



   /**
    * A method which should be used to verify that the driver is really on a
    * given route.
    * <p>
    * The route URL and page contents should be checked here.
    * 
    * @return The page object representing the result of the route.
    */
   public abstract T verify_route();


   /**
    * A method which checks that the currently-logged-in user is <i>expected</i>
    * 
    * @param expected
    *           The user you expect to be logged in as.
    * @return This page object.
    */
   public Page<T> check_current_user_is(String expected) {
      report_step(generate_step_descriptor());
      Assert.assertEquals(driver.findElement(By.id(NAVBAR_USER_ID)).getText().split("/")[1], expected);
      return this;
   }

   /**
    * A helper method that waits 5 seconds for the passed-in ID to disappear.
    * Useful for handling modal dismissal - Give it the ID of the modal and it
    * will wait until it's gone before trying to do anything else.
    * 
    * @param id
    *           The ID of the element to wait to go away
    */
   protected void wait_for_disappearance_of(String id) {
      wait_for_disappearance_Of(id, 900);
   }

   /**
    * A helper method that waits for the passed-in ID to disappear. Useful for
    * handling modal dismissal - Give it the ID of the modal and it will wait
    * until it's gone before trying to do anything else.
    * 
    * @param id
    *           The ID of the element to wait to go away
    * @param timeOutInSeconds
    *           The number of seconds to wait, at most
    */
   protected void wait_for_disappearance_Of(String css, int timeOutInSeconds) {
       try {
           WebDriverWait wait = new WebDriverWait(driver,900);
           wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(css)));
       }catch (Exception e){
           e.printStackTrace();

       }

   }

   /**
    * A helper method that waits for the passed-in ID to appear. Useful for
    * handling the cases when user has to wait till the loading of an element.
    * 
    * @param name
    *           The ID of the element to get loaded
    * @param timeOutInSeconds
    *           The number of seconds to wait, at most
    */
   protected void wait_for_appearance_of_name(String name) {
	 
	   WebDriverWait wait = new WebDriverWait(driver,900);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
     
   }

   protected void wait_for_appearance_of_xpath(String xpathExpression) {
	   WebDriverWait wait = new WebDriverWait(driver,900);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathExpression)));
   }
   protected void waitForAppearanceOfClass(String className) {
	   WebDriverWait wait = new WebDriverWait(driver,900);
	      wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(className)));
	   }

   /**
    * A helper method that waits 5 seconds for the passed-in ID to appear.
    * Useful for handling the cases when user has to wait till the loading of an
    * element.
    * 
    * @param id
    *           The ID of the element to get loaded
    */
   protected void wait_for_appearance_of(String id) {
      wait_for_appearance_of(id);
   }

   protected void wait_for_element_to_have_text(WebElement element, String text) {
      wait_for_element_to_have_text(element, text, 1);
   }

   protected void wait_for_element_to_have_text(WebElement element, String text, int timeout) {
      String t = element.getText();
      int i = 0;
      boolean cont = true;
      while (++i < timeout * 10 && cont) {
         try {
            Thread.sleep(100);
         } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
         }
         t = element.getText();
         if (!t.contains(text)) {
            cont = true;
         }
      }
      Assert.assertTrue(t.contains(text), "Message [" + t + "] did not contain [" + text + "] after [" + i
            + "] sleeps(100).");
   }

   protected void wait_for_element_to_be_not_empty_value(WebElement element, int timeout) {
      String t = get_text(element);
      int i = 0;
      while (++i < timeout * 10 && t.isEmpty()) {
         try {
            Thread.sleep(100);
         } catch (InterruptedException e) {
        	 Thread.currentThread().interrupt();
         }
         t = get_text(element);
      }
      Assert.assertTrue(t.length() > 0, "Element was still empty after [" + i + "] sleeps(100).");

   }
  /**
   Method to get the status of a button i.e. button is enabled or disabled.
   It will return boolean true if button is enable else it will return false.
   **/
   
   protected boolean check_enablement_of_button(WebElement element) {
	   WebElement btn=element;
	   boolean btnStatus=btn.isEnabled();
	   return btnStatus;
	   }
   
   /**
    * Helper method for selecting entries in a UI grid.
    * 
    * @param uiGrid
    *           The WebElement representing the UI grid itself
    * @param searchValue
    *           The String value to search for
    * @param colIndex
    *           The column in which to search for searchValue
    * @return The WebElement representing the first row matching the search
    *         string, or null if it's not found.
    */
   protected WebElement get_element_from_grid_by_string_at_column(WebElement uiGrid, String searchValue, int colIndex) {
      final String step = generate_step_descriptor();
      report_step(step);
      List<WebElement> gridEntries = uiGrid.findElements(By.className("ui-grid-row"));

      final StringBuilder sb = new StringBuilder();
      sb.append("Values for rows at col " + colIndex + ": ");
      int i = 0;
      for (WebElement entry : gridEntries) {
         String[] entryCols = entry.getText().toLowerCase().split("\n");
         sb.append("row " + (++i) + ": " + entry.getText());
         if (entryCols[colIndex].equals(searchValue.toLowerCase())) {
            return entry;
         }
      }
      report_step(step + " Did not find searchValue " + searchValue + ". " + sb.toString());
      return null;
   }

   protected void wait_for_element_to_have_value(WebElement element, String text, int timeout) {
      String t = element.getAttribute("value");
      int i = 0;
      while (++i < timeout * 10 && t.isEmpty()) {
         try {
            Thread.sleep(100);
         } catch (InterruptedException e) {
        	 Thread.currentThread().interrupt();
         }
         t = element.getAttribute("value");
      }
      Assert.assertTrue(t.contains(text), "Element [" + t + "] did not contain value [" + text + "] after [" + i
            + "] sleeps(100).");
   }

   protected void waitForElementToBeEnabled(WebElement element, int timeout) {
      report_step(generate_step_descriptor());
      boolean enabled = element.isEnabled();
      int i = 0;
      while (++i < timeout * 10 && !enabled) {
         try {
            Thread.sleep(100);
         } catch (InterruptedException e) {
        	 Thread.currentThread().interrupt();
         }
         enabled = element.isEnabled();
      }
      Assert.assertTrue(enabled, "Element " + element + " did not become enabled after [" + i + "] checks.");

   }

   protected void waitForElementToBeNotEmpty(WebElement element) {
      waitForElementToBeNotEmpty(element, 1);
   }
   
   protected void waitForButtonToBeClickable(WebElement element) {
	   WebDriverWait wait = new WebDriverWait(driver,60);
	   wait.until(ExpectedConditions.elementToBeClickable(element));
	   }
   
   protected void waitForElementToBeNotEmpty(WebElement element, int timeout) {
      String t = element.getText();
      int i = 0;
      while (++i < timeout * 10 && t.isEmpty()) {
         try {
            Thread.sleep(100);
         } catch (InterruptedException e) {
        	 Thread.currentThread().interrupt();
         }
         t = element.getText();
      }
      Assert.assertTrue(t.length() > 0, "Element was still empty after [" + i + "] sleeps(100).");

   }

   protected void wait_for_disappearance_of_modal() {
      // It takes about a second for modals to dismiss.
      try {
         Thread.sleep(2000);
      } catch (InterruptedException ie) {
    	  Thread.currentThread().interrupt();
      }
      // waitForLoadCompletion();
   }
   
   public void waitForPageLoad() {
		wait_for_disappearance_Of(LOADING_INDICATOR, 900);
	}

   /**
    * A convenience method to return a {@link WebDriverWait} with the driver
    * set.
    * <p>
    * Equivalent to (new WebDriverWait(driver, timeOutInSeconds))...
    * </p>
    * 
    * @param timeOutInSeconds
    *           the time before the driver throws an exception
    * @return the element
    * @see WebDriverWait
    */
   protected WebDriverWait webDriverWait(long timeOutInSeconds) {
      return new WebDriverWait(driver, timeOutInSeconds);
   }

   /**
    * Wait for angular requests to finish and for the Loading Bar to disappear.
    * This is a good indicator that a page has finished loading.
    */
   protected void wait_for_load_completion() {
      final String step = generate_step_descriptor();
      report_step(step);
      try {
         /*
          * new NgWebDriver((JavascriptExecutor)
          * driver).waitForAngularRequestsToFinish();
          * webDriverWait(10).until(ExpectedConditions.attributeContains(
          * By.cssSelector("body > tek-progress-bar > .progress"), "class",
          * "full-bar"));
          */
      } catch (TimeoutException e) {
         report_step(step + "Loading bar did not complete on page " + URL_PATH);
      }
   }

   protected void wait_for_url(final String url, final String message) {
      try {
         webDriverWait(10).withMessage(message).until(urlMatches(url + "(\\?.+)*$"));
      } catch (TimeoutException e) {
         throw new TimeoutException(message + "Actual URL: " + driver.getCurrentUrl() + ", expected URL : " + url, e);
      }
   }

   /**
    * Reports the given string as a test step to the test reporter. Appears in
    * generated reports.
    * 
    * @param step
    *           a string describing the step being performed
    */
   protected void report_step(String step) {
      Reporter.log(step);
   }

   /**
    * Generates a descriptor based on the method calling.
    * <p>
    * Example: If called from the enter_username method in _LoginPage, this
    * would return '_LoginPage -> enter_username'
    * 
    * @return the method name descriptor
    */
   protected String generate_step_descriptor(Object... parameter) {
      StackTraceElement callee = Thread.currentThread().getStackTrace()[2];
      String calleeClassName = callee.getClassName();
      calleeClassName = calleeClassName.substring(calleeClassName.lastIndexOf('.') + 1); // Remove
                                                                                         // the
                                                                                         // package.
      return calleeClassName + " -> " + callee.getMethodName();
   }

   /**
    * Returns the fully qualified URL for a route. Based on configured base url.
    * Example: /login --> http://localhost:3000/login
    * 
    * @param partialRoute
    *           the partial route for the page
    * @return the fully qualified URL with the protocol, host, port... appended
    *         with the partial route.
    */
   protected String build_qualified_url(String partialRoute) {
      return SeleniumTest.getTargetBaseURL().replaceAll("\\/$", "") + partialRoute; // baseurl
                                                                                    // +
                                                                                    // route
                                                                                    // (removing
                                                                                    // the
                                                                                    // trailing
                                                                                    // '/')
   }
   protected String get_text(WebElement id) {
      report_step(generate_step_descriptor());
      return id.getAttribute("value");
   }


   


   
}
