package com.org.e2e.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.org.e2e.report.ScreenshotListener;
import org.testng.annotations.Test;

/**
 * Base test for a selenium test.
 * <p>
 * Performs test setup, property loading, web driver configuration, url targets.
 * Also contains convenience methods to assist in writing cleaner tests.
 */
public class SeleniumTest {

	private static final String PARAMETER_PROPERTY_FILE_DIR = "resources/parameters/";
	private static final String PARAMETER_PROPERTY_FILE_EXTENSION = ".params.properties";
	private static final String SELENIUM_HOST_URL_KEY = "selenium.host.url";
	private static final String SELENIUM_BROWSER_NAME_KEY = "selenium.browser.name";
	private static final String PROJECT_URL_KEY = "project.url";
	private static final String BROWSER_STACK_USERNAME = "browserStack_username";
	private static final String BROWSER_STACK_AUTOMATE_KEY = "browserStack_accessKey";
	private static final String BUILD_NAME = "octopus_build_name";
	private static final String BROWSERSTACK_RUN = "browserstack.run";
	public WebDriver driver;
	private static String seleniumServerURL = "http://localhost:4444/wd/hub"; // default
	private static String seleniumBrowserName = "chrome"; // default
	private static String targetBaseURL = "https://www.google.com/"; // default
	private static String browserstackRun = "false"; // default
	public static String browserstackUsername = "vikashnowal1"; // default
	public static String browserstackAutomateKey = "9iSAVhFNcYnCVKLEKq6z"; // default
	public static String sessionID = "";
	public static String buildName = "testbuild";
	public static String URL = "https://" + browserstackUsername + ":" + browserstackAutomateKey
			+ "@hub-cloud.browserstack.com/wd/hub";

	private static Properties loadPropertyFile(InputStream input) {
		if (input == null)
			throw new IllegalArgumentException("Must have a valid file to load properties from.");

		Properties properties = new Properties();
		try {
			// load a properties file
			properties.load(input);
		} catch (IOException ex) {
			throw new RuntimeException("Unable to load properties file!", ex);
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return properties;
	}

	/**
	 * Base url of the selenium server. Example: http://localhost:4444/wd/hub
	 * 
	 * @return the selenium server url
	 */
	public static String getSeleniumServerURL() {
		return seleniumServerURL;
	}

	/**
	 * Base url of the application. Example: http://localhost:3000/
	 * 
	 * @return the base url string
	 */
	public static String getTargetBaseURL() {
		return targetBaseURL;
	}

	/**
	 * Load all the parameters in the parameters directory into system properties.
	 */
	@BeforeSuite(alwaysRun = true)
	public void load_parameters() {
		loadClasspathPropertyFiles(PARAMETER_PROPERTY_FILE_DIR, PARAMETER_PROPERTY_FILE_EXTENSION);
	}

	/**
	 * Load system properties.
	 */
	@BeforeSuite(alwaysRun = true)
	public void load_config_properties() {

		loadClasspathPropertyFiles(PARAMETER_PROPERTY_FILE_DIR, PARAMETER_PROPERTY_FILE_EXTENSION);
		if (System.getProperty(SELENIUM_HOST_URL_KEY) != null)
			seleniumServerURL = System.getProperty(SELENIUM_HOST_URL_KEY);

		if (System.getProperty(SELENIUM_BROWSER_NAME_KEY) != null)
			seleniumBrowserName = System.getProperty(SELENIUM_BROWSER_NAME_KEY);

		if (System.getProperty(PROJECT_URL_KEY) != null) {
			targetBaseURL = System.getProperty(PROJECT_URL_KEY);
		}

		if (System.getProperty(BROWSERSTACK_RUN) != null) {
			browserstackRun = System.getProperty(BROWSERSTACK_RUN);
		}

		if (System.getProperty(BROWSER_STACK_USERNAME) != null)
			browserstackUsername = System.getProperty(BROWSER_STACK_USERNAME);

		if (System.getProperty(BROWSER_STACK_AUTOMATE_KEY) != null)
			browserstackAutomateKey = System.getProperty(BROWSER_STACK_AUTOMATE_KEY);

		if (System.getProperty(BUILD_NAME) != null)
			buildName = System.getProperty(BUILD_NAME);

	}

	@BeforeSuite(alwaysRun = true, timeOut = 250000, dependsOnMethods = { "load_parameters", "load_config_properties" })
	public void open_web_driver() throws MalformedURLException, InterruptedException {
		URL host;
		try {
			host = new URL(seleniumServerURL);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		if (browserstackRun.equals("false")) {
			try {
				String osName = System.getProperty("os.name");
				if (osName.contains("Windows")) {
					if (seleniumBrowserName.equalsIgnoreCase("firefox")) {
						File file = new File("drivers\\geckodriver.exe");
						System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
						DesiredCapabilities capabilities = new DesiredCapabilities();
						capabilities.setCapability("marionette", true);
						driver = new FirefoxDriver(capabilities);
					} else if (seleniumBrowserName.equalsIgnoreCase("chrome")) {
						ChromeOptions options = new ChromeOptions();
						options.setExperimentalOption("excludeSwitches",
								Collections.singletonList("enable-automation"));
						options.addArguments("disable-infobars");
						options.addArguments("-incognito");
						options.addArguments("start-maximized");
						options.addArguments("--disable-extensions");
						// options.setCapability("requireWindowFocus", true);
						File file = new File("drivers/chromedriver_new.exe");
						System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
						driver = new ChromeDriver(options);

					} else if (seleniumBrowserName.equalsIgnoreCase("IE")) {
						File file = new File("drivers/IEDriverServer.exe");
						System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
						DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
						capabilities.setCapability(
								InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
						capabilities.setCapability("requireWindowFocus", true);
						driver = new InternetExplorerDriver(capabilities);
					} else if (seleniumBrowserName.equalsIgnoreCase("edge")) {
						File file = new File("drivers/MicrosoftWebDriver.exe");
						System.setProperty("webdriver.edge.driver", file.getAbsolutePath());
						DesiredCapabilities capabilitiesEdge = DesiredCapabilities.edge();
						driver = new EdgeDriver(capabilitiesEdge);

					}
				} else if (osName.contains("Linux")) {
					if (seleniumBrowserName.equalsIgnoreCase("firefox")) {

						File file = new File("drivers/geckodriver-linux");
						System.setProperty("webdriver.firefox.marionette", file.getAbsolutePath());
						DesiredCapabilities capabilities = new DesiredCapabilities();
						capabilities.setCapability("marionette", true);
						driver = new FirefoxDriver(capabilities);
					} else if (seleniumBrowserName.equalsIgnoreCase("chrome")) {
						ChromeOptions options = new ChromeOptions();
						options.setExperimentalOption("excludeSwitches",
								Collections.singletonList("enable-automation"));
						options.addArguments("disable-infobars");
						options.addArguments("-incognito");
						// options.setCapability("requireWindowFocus", true);
						options.addArguments("--no-sandbox");
						File file = new File("drivers/chromedriver-linux");
						System.out.println("****************" + file.getAbsolutePath());
						System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
						driver = new ChromeDriver(options);
					}
				}

				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				// driver.manage().window().maximize();
				driver.get(targetBaseURL);
				// driver.navigate().refresh();
				Thread.sleep(6000);
			} catch (WebDriverException e) {
				System.out.println(e.getMessage());
			} catch (Throwable t) {
				// Selenium puts blank lines in its exceptions. Remove them and
				// rethrow.
				throw new RuntimeException(t.getMessage().replaceAll("(?m)^[ \t]*\r?\n", ""));
			}
		} else {
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String browserName = System.getProperty("browserStack.browser.name");
			String browserVersion = System.getProperty("browserStack.browser.version");
			String OS_Name = System.getProperty("browserStack.os.name");
			String osVersion = System.getProperty("browserStack.os.versions");
			String resolution = System.getProperty("browserStack.resolution");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("project", "My_Framework");
			capabilities.setCapability("name", "UI_Automation_" + timeStamp);
			capabilities.setCapability("browserstack.local", false);
			capabilities.setCapability("browserstack.debug", true);
			capabilities.setCapability("browserstack.console", "verbose");
			capabilities.setCapability("browserstack.networkLogs", true);
			capabilities.setCapability("browserstack.video", true);
			capabilities.setCapability("resolution", resolution);
			capabilities.setCapability("browser", browserName);
			capabilities.setCapability("browser_version", browserVersion);
			capabilities.setCapability("os", OS_Name);
			capabilities.setCapability("os_version", osVersion);
			capabilities.setCapability("build", buildName);

			if (browserName.equals("firefox"))
				capabilities.setCapability("browserstack.geckodriver", "0.10.0");

			driver = new RemoteWebDriver(new URL(URL), capabilities);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(targetBaseURL);
			driver.navigate().refresh();
			((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
			sessionID = ((RemoteWebDriver) driver).getSessionId().toString();
		}

	}

	/**
	 * Attempts to close browser always after execution all the test cases Method
	 * call to mark test cycle in browser stack as pass/fail
	 * 
	 * @throws Exception
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 */

	@AfterSuite(alwaysRun = true)
	public void close_web_driver() throws UnsupportedEncodingException, URISyntaxException, Exception {
		if (browserstackRun.equals("true"))
			setTestStatusOnBrowserStack(ScreenshotListener.testStatus);
		if (driver != null)
			driver.quit();
	}

	/**
	 * @param status
	 * @throws URISyntaxException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 *             Method to mark the test cycle in browser stack as passed or
	 *             failed after test completion.
	 * 
	 */
	public static void setTestStatusOnBrowserStack(String status)
			throws URISyntaxException, UnsupportedEncodingException, IOException {

		// Rest api call to change the status of test cycle as pass/fail

		URI uri = new URI("https://" + browserstackUsername + ":" + browserstackAutomateKey
				+ "@api.browserstack.com/automate/sessions/" + sessionID + ".json");
		HttpPut putRequest = new HttpPut(uri);
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add((new BasicNameValuePair("status", status)));
		nameValuePairs.add((new BasicNameValuePair("reason", "")));
		putRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpClientBuilder.create().build().execute(putRequest);
	}

	/**
	 * Skips the test if it has not yet been implemented.
	 */
	protected void skip_not_implemented() {
		throw new SkipException("Skipping test. Not yet implemented.");
	}

	/*
	 * protected boolean currentUserIs(String expected) { return
	 * driver.findElement(By.id(NAVBAR_USER_ID)).getText().split("/")[1].equals(
	 * expected); }
	 */

	/**
	 * Loads all properties files in the given resource directory with the given
	 * file extension.
	 * 
	 * @param resourceFileDir
	 *            The resource directory in which to load all files from.
	 * @param resourceFileExtension
	 *            The extension of the file to load.
	 */
	private final void loadClasspathPropertyFiles(final String resourceFileDir, final String resourceFileExtension) {
		String path = getClass().getClassLoader().getResource("parameters/").getPath();

		// Get a list of property files with the given extension.
		File[] propertiesFiles = new File(path).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(resourceFileExtension);
			}
		});

		// For each file, open it and load each property in the file into the
		// system properties.
		for (File file : propertiesFiles) {
			try {
				final Properties properties = loadPropertyFile(new FileInputStream(file));

				for (String key : properties.stringPropertyNames()) {
					System.setProperty(key, properties.getProperty(key));
				}
			} catch (FileNotFoundException e) {
				throw new IllegalArgumentException("Unable to find property file.", e);
			}
		}
	}

}
